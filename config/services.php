<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1053478688132341',
        'client_secret' => '48f574f3711be599b8227237c3f6d961',
        'redirect' => 'http://fourbtec.dev/callback',
    ],

    'google' => [
        'client_id' => '31037302436-hcr0ckp0umo9p9gb38ts1r9gecjrff6j.apps.googleusercontent.com',
        'client_secret' => '3Qj7M6Fc1maZzEv-qs4Y3Pmg',
        'redirect' => 'http://fourbtec.dev/callback/google',
    ],

    'twitter' => [
        'client_id' => 'E1iGkcyTP3i3cA1kGm6cVqO7y',
        'client_secret' => 'oL8C3bU8X21n1g8PspHNYqCHYACkfwpswoq8gLXOyQ0t1uuP8Q',
        'redirect' => 'http://fourbtec.dev/callback/twitter',
    ],

    'pinterest' => [
        'client_id' => '4887486270741165724',
        'client_secret' => '8291109d5f5533b4a55d20a3d5d8bac6760e991eb1d50c5bc11fa0da1737e65e',
        'redirect' => 'http://mizanur-milon.com/callback/pinterest',
    ],


];
