@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">My Favourite Product List</div>
                <div class="panel-body">                  
                    
                      <table class="table table-condensed">
                        <thead>
                          <tr>
                              <th>S.L</th>
                              <th>Product Name</th>                          
                          </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;?>
                            @forelse($favorite_products as $product)
                            <?php $i++;?>
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$product->listedAdd->title}}</td>
                                 </tr>
                            @empty
                            @endforelse

                     </tbody>
                      </table>                   

                </div>
            </div>
        </div>
    </div>
</div>
@endsection


