@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Listed Add</div>
                <div class="panel-body">
                  
                   @if(Session::has('success'))
                        <div class="alert alert-success {{ session()->has('important-msg') ? 'important' : '' }}">
                            @if(Session::has('important-msg'))
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            @endif
                            <strong>Success!</strong>
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    @if(Session::has('error'))
                        <div class="alert alert-danger {{ session()->has('important-msg') ? 'important' : '' }}">
                            @if(Session::has('important-msg'))
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            @endif
                            <strong>Error!</strong>
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="{{ url('/listed-ad-post') }}">
                        {{ csrf_field() }} 

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}">

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('ad_category_id') ? ' has-error' : '' }}">
                            <label for="ad_category_id" class="col-md-4 control-label">Category</label>

                            <div class="col-md-6">
                                <select name="ad_category_id" id="ad_category_id" class="form-control">
                                    <option value="" selected>Please select category</option>
                                    @forelse($categories as $category)
                                       <option value="{{$category->id}}"
                                        @if(old('ad_category_id')==$category->id)
                                            selected
                                        @endif>{{ $category->name }}
                                    @empty
                                        Not Found
                                    @endforelse    
                                </select> 
                                @if ($errors->has('ad_category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ad_category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                            <label for="location" class="col-md-4 control-label">Location</label>
                            <div class="col-md-6">
                                <input id="location" type="text" class="form-control" name="location" value="{{ old('location') }}">

                                @if ($errors->has('location'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('') ? ' has-error' : '' }}">
                            <label for="location" class="col-md-4 control-label"> Feature Image</label>
                            <div class="col-md-6">
                                <input id="location" type="file" required="required" class="form-control" name="feature_image" />
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('') ? ' has-error' : '' }}">
                            <label for="location" class="col-md-4 control-label"> Photos(10 for free)</label>
                            <div class="col-md-6">
                                <input id="location" type="file" multiple="multiple" class="form-control" name="images[]" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="condition" class="col-md-4 control-label"></label>
                            <div class="col-md-6">Fill in the details 
                                <hr>
                            </div>                           
                        </div>

                        <div class="form-group{{ $errors->has('condition') ? ' has-error' : '' }}">
                            <label for="condition" class="col-md-4 control-label">Condition</label>
                            <div class="col-md-6"> 
                                <select name="condition" class="form-control">
                                    @forelse($conditions as $condtion)
                                        <option value="{{$condtion->id}}">{{ucfirst($condtion->name)}}</option>
                                    @empty
                                        Not Found
                                    @endforelse    
                                </select> 
                                @if ($errors->has('condition'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('condition') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       <div class="form-group{{ $errors->has('brand_id') ? ' has-error' : '' }}">
                            <label for="brand_id" class="col-md-4 control-label">Brand</label>

                            <div class="col-md-6">
                                <select name="brand_id" id="brand-dropdown" class="form-control"></select>
                                <div class="other-field"></div>
                            </div>
                        </div>                        

                        <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                            <label for="short_description" class="col-md-4 control-label">Features(optional)</label>
                            <div class="col-md-6" id="category-feature">
                            </div>
                        </div>  

                        <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                            <label for="short_description" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <textarea name="short_description" class="form-control">{{ old('short_description') }}</textarea>

                                @if ($errors->has('short_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('short_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 

                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Price</label>

                            <div class="col-md-6">
                                <input id="number" type="number" class="form-control" name="price" value="{{ old('price') }}">

                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif

                                <input type="checkbox" name="negotiable" value="yes"> Negotiable

                            </div>
                        </div> 

                        <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                            <label for="company_name" class="col-md-4 control-label">Company Name</label>

                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control" name="company_name" value="{{ old('company_name') }}">    
                            </div>
                        </div>      

                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="phone_number" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <div id="add-more-field">
                                @if(sizeof(old('phone_number'))>0)
                                    @foreach(old('phone_number') as $number)
                                        <input id="phone_number" type="number" class="form-control" name="phone_number[]" value="{{$number}}"> 
                                    @endforeach
                                @else
                                    <input id="phone_number" type="number" class="form-control" name="phone_number[]">    
                                @endif         
                                    <input type="checkbox" name="number_status" value="no"> Hide
                                   phone number(s)
                                </div>    

                                <div class="col-md-6 col-md-offset-4 pull-right">
                                    <button type="button" class="btn pull-right add-more-mbl">
                                        <i class="fa fa-btn fa-user"></i> Add More
                                    </button>
                                </div>

                                
                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>      

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{Auth::user()->email}}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>      

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_script')
    <script>
        $(document).ready(function() {
            $('#ad_category_id').change(function() {
                var ad_category_id = $('#ad_category_id').val();
                var token = "{{csrf_token()}}";
                if(ad_category_id !='') {
                
                    $.ajax({
                        url: "{{url('/get-brand-feature')}}",
                        type: 'post',
                        dataType: 'JSON',
                        data: {ad_category_id:ad_category_id,_token:token},
                        success: function(returnData) {  
                             
                            $('#brand-dropdown').empty();
                            $('#category-feature').empty();

                            if(returnData.brands.length>0) {                                
                                $.each(returnData.brands, function(index,brand) { 
                                    var brandDrpdown = '<option value="'+brand.id+'">'+brand.name+'</option>'; 
                                    $('#brand-dropdown').append(brandDrpdown);
                                }); 
                                    $('#brand-dropdown').append('<option value="">Other</option>');  

                            } else {
                                var brandDrpdown = '<option value="">Brand Not Found</option>'; 
                                $('#brand-dropdown').append(brandDrpdown);
                            }   

                            if(returnData.category_features.length>0) {                                
                                $.each(returnData.category_features, function(index,feature) { 
                                    var featureDrpdown = ' <input type="checkbox" name="feature_id[]" value="'+feature.id+'"> '+ feature.name; 
                                    $('#category-feature').append(featureDrpdown);
                                });  
                            } else {
                                var featureDrpdown = 'Feature Not Found'; 
                                $('#category-feature').append(featureDrpdown);
                            }                             
                        }
                   });                
                }
            });

            $(document).ready(function() {          
                var ad_category_id = $('#ad_category_id').val();
                var token = "{{csrf_token()}}";
                if(ad_category_id !='') {
                
                    $.ajax({
                        url: "{{url('/get-brand-feature')}}",
                        type: 'post',
                        dataType: 'JSON',
                        data: {ad_category_id:ad_category_id,_token:token},
                        success: function(returnData) {  
                             
                            $('#brand-dropdown').empty();
                            $('#category-feature').empty();

                            if(returnData.brands.length>0) {                                
                                $.each(returnData.brands, function(index,brand) { 
                                    var brandDrpdown = '<option value="'+brand.id+'">'+brand.name+'</option>'; 
                                    $('#brand-dropdown').append(brandDrpdown);
                                }); 
                                    $('#brand-dropdown').append('<option value="">Other</option>');  

                            } else {
                                var brandDrpdown = '<option value="">Brand Not Found</option>'; 
                                $('#brand-dropdown').append(brandDrpdown);
                            }   

                            if(returnData.category_features.length>0) {                                
                                $.each(returnData.category_features, function(index,feature) { 
                                    var featureDrpdown = ' <input type="checkbox" name="feature_id[]" value="'+feature.id+'"> '+ feature.name; 
                                    $('#category-feature').append(featureDrpdown);
                                });  
                            } else {
                                var featureDrpdown = 'Feature Not Found'; 
                                $('#category-feature').append(featureDrpdown);
                            }                             
                        }
                   });                
                }
            });

            $('.add-more-mbl').click(function() { 
                $('#add-more-field').prepend('<input id="phone_number" type="text" class="form-control" name="phone_number[]">');
            });

            $('#brand-dropdown').change(function(){
                var brand_val = $('#brand-dropdown').val();
                if(brand_val=='') {
                    $('.other-field').append('<input type="text" class="form-control" name="other">');
                }else {
                    $('.other-field').empty();
                }
            });
        });
    </script>
@endsection
