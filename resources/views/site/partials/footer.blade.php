<!---->
{{-- 	<div class="footer">
		<p class="footer-class">© 2015 Mihstore All Rights Reserved | Template by  <a href="{{url('/')}}" target="_blank">4Btech</a> </p>
		
			<a href="#home" class="scroll to-Top" >  GO TO TOP!</a>
	<div class="clearfix"> </div>
	</div> --}}

<div class="footer">
   <footer class="container-fluid text-center">
        <div class="container">
            <div class="row farsi footerLink hidden-sm hidden-xs">
               
                    <div class="col-md-2 Rfloat hidden-xs hidden-sm">
                        <a href="#" target="_self" id="BottomStoreListLink"><font><font>Store name list of </font></font></a><br>
                        <a href="#" target="_self" id="BottomConfirmItemsLink"><font><font>confirmed receipt of the goods </font></font></a><br>
                        <a href="#" target="_self" id="BottomSellerProtectionLink"><font><font>support of </font></font></a><br>
                        <a href="#" target="_self" id="BottomWeblogLink"><font><font>Isam blog </font></font></a><br>
                        <a href="#" target="_self" id="BottomSuggestionsLink"><font><font>feedback </font></font></a><br>
                        <a href="#" target="_self" id="BottomForbiddenItemLink"><font><font>and exchanges of prohibited goods list</font></font></a><br>
                    </div>
                    <div class="col-md-2 Rfloat hidden-xs hidden-sm">
                        <a href="#" target="_self" id="BottomDisputeRulsLink"><font><font>Laws related to disputes </font></font></a><br>
                        <a href="#" target="_self" id="BottomContactUsLink"><font><font>Contact Isam </font></font></a><br>
                        <a href="#" target="_self" id="BottomInviteFriendsLink"><font><font>Isam invite friends to </font></font></a><br>
                        <a href="#" target="_self" id="BottomFAQLink"><font><font>frequently asked questions </font></font></a><br>
                        <a href="#" target="_self" id="BottomWhatIsShebaLink"><font><font>What is the number of Sheba? </font></font></a><br>
                        <a href="#" target="_self" id="BottomBidOnAuctionLink"><font><font>Participate in auctions</font></font></a>
                    </div>
                    <div class="col-md-4 Rfloat hidden-xs hidden-sm">
                        <a href="#" target="_self" id="BottomHomePageLink"><font><font>About Isam </font></font></a><br>
                        <a href="#" target="_self" id="BottomEsamRegistrationLink"><font><font>Isam membership of the </font></font></a><br>
                        <a href="#" target="_self" id="BottomBuyBenefitLink"><font><font>advantages of buying goods from Isam </font></font></a><br>
                        <a href="#" target="_self" id="BottomSellBenefitLink"><font><font>Isam benefits of selling goods on </font></font></a><br>
                        <a href="#" target="_blank" id="BottomItemTrackingLink"><font><font>intercept postal Export </font></font></a><br>
                        <a href="#" target="_blank" id="BottomHelp"><font><font>Help</font></font></a>
                    </div>

                     <div class="col-sm-6 col-md-2 Rfloat text-center">
                        
				    <center>    
				        <img id="drfthwladrftwmcsnbpe" class="img-responsive" style="cursor:pointer" onclick="window.open(&quot;http://trustseal.enamad.ir/Verify.aspx?id=14182&amp;p=nbpdodshnbpdaqgwwkyn&quot;, &quot;Popup&quot;,&quot;toolbar=no, location=no, statusbar=no, menubar=no, scrollbars=1, resizable=0, width=580, height=600, top=30&quot;)" alt="" src="{{asset('/site-assets/images/footer-logo.png')}}">
				    </center>

                    </div>
                    <div class="col-sm-6 col-md-2 clearLRPadding Rfloat text-left">
                        <img id="jxlzfukzfukzoeukwlao" style="cursor: pointer; margin-right: -50px;" onclick="window.open(&quot;https://logo.samandehi.ir/Verify.aspx?id=16684&amp;p=rfthgvkagvkamcsiaods&quot;, &quot;Popup&quot;,&quot;toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=450, height=630, top=30&quot;)" alt="logo-samandehi" src="{{asset('/site-assets/images/footer-logo1.png')}}">
                    </div>
                    
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 text-right hidden-xs">
                        <img id="EsamIconLit" src="{{asset('/site-assets/images/esamIconLit.jpg')}}" alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="row">
                            <div id="androidIcons" class="col-sm-7 clearLRPadding">
                                <a href="">
                                    <img id="bazarIcon" src="{{asset('/site-assets/images/badge-bazaar-gray.jpg')}}" alt="">
                                </a>
                                <a href="">
                                    <img id="androidIcon" src="{{asset('/site-assets/images/badge-esam-gray.jpg')}}" alt="">
                                </a>
                            </div>
                            <div id="SocialIcons" class="col-sm-5 text-left clearLRPadding">
                                <a href="">
                                    <img id="telegramIcon" src="{{asset('/site-assets/images/telegram.png')}}" alt="">
                                </a>
                                <a href="">
                                    <img id="linkedinIcon" src="{{asset('/site-assets/images/linkedin.png')}}" alt="">
                                </a>
                                <a href="http://goo.gl/QGCTx2">
                                    <img id="InstagramIcon" src="{{asset('/site-assets/images/Instagram.png')}}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 farsi">
                        <p class="footerText text-muted"><font><font>
                            Use of the site is conditional on acceptance of User Agreement and Privacy Policy. </font></font><br><font><font>
                            All Rights Reserved This collection belongs to Business Information Systems Technology Development Co., Ltd under number 339 393 is Sam Vieira.
                        </font></font></p>
                    </div>
                </div>
            </div>
            <div class="scrollToTop" style="display: block;"><span class="glyphicon glyphicon-chevron-up"></span></div>

            <a href="#home" class="scroll to-Top" >  GO TO TOP!</a>
        </footer>

        
      </div>
   </div>
</div>
<!---->