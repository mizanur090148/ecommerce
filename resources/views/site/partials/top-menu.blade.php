<div class="header-bottom">
	<div class="top-nav">		
	  <ul class="megamenu skyblue">
		    <li class="active grid"><a  href="{{url('/')}}">Home</a></li>
		    @forelse($categories as $category)
		        @if($category->menu=='yes')
                    <li class="active grid"><a  href="{{url('/view-products/'.$category->id)}}">{{$category->name}}</a></li>
                @endif    
            @empty            
            @endforelse    

            {{-- <li class="active grid"><a  href="#">Collectible</a></li>
		     <li class="active grid"><a  href="#">PlayStation and Xbox</a>
			    <div class="megapanel">
				<div class="row">
					<div class="col1">
						<div class="h_nav">
							<ul>
								<li><a href="store.html">Accessories</a></li>
								<li><a href="store.html">Bags</a></li>
								<li><a href="store.html">Caps & Hats</a></li>
								<li><a href="store.html">Hoodies & Sweatshirts</a></li>
								<li><a href="store.html">Jackets & Coats</a></li>
								<li><a href="store.html">Jeans</a></li>
								<li><a href="store.html">Jewellery</a></li>
								<li><a href="store.html">Jumpers & Cardigans</a></li>
								<li><a href="store.html">Leather Jackets</a></li>
								<li><a href="store.html">Long Sleeve T-Shirts</a></li>
								<li><a href="store.html">Loungewear</a></li>
							</ul>	
						</div>							
					</div>
					<div class="col1">
						<div class="h_nav">
							<ul>
								<li><a href="store.html">Shirts</a></li>
								<li><a href="store.html">Shoes, Boots & Trainers</a></li>
								<li><a href="store.html">Shorts</a></li>
								<li><a href="store.html">Suits & Blazers</a></li>
								<li><a href="store.html">Sunglasses</a></li>
								<li><a href="store.html">Sweatpants</a></li>
								<li><a href="store.html">Swimwear</a></li>
								<li><a href="store.html">Trousers & Chinos</a></li>
								<li><a href="store.html">T-Shirts</a></li>
								<li><a href="store.html">Underwear & Socks</a></li>
								<li><a href="store.html">Vests</a></li>
							</ul>	
						</div>							
					</div>
					<div class="col1">
						<div class="h_nav">
							<h4>Popular Brands</h4>
							<ul>
								<li><a href="store.html">Levis</a></li>
								<li><a href="store.html">Persol</a></li>
								<li><a href="store.html">Nike</a></li>
								<li><a href="store.html">Edwin</a></li>
								<li><a href="store.html">New Balance</a></li>
								<li><a href="store.html">Jack & Jones</a></li>
								<li><a href="store.html">Paul Smith</a></li>
								<li><a href="store.html">Ray-Ban</a></li>
								<li><a href="store.html">Wood Wood</a></li>
							</ul>	
						</div>												
					</div>
				  </div>
				</div>
			</li>
			
		    <li class="grid"><a  href="#">Watches and Jewelery</a>
			   <div class="megapanel">
				<div class="row">
					<div class="col1">
						<div class="h_nav">
							<ul>
								<li><a href="store.html">Accessories</a></li>
								<li><a href="store.html">Bags</a></li>
								<li><a href="store.html">Caps & Hats</a></li>
								<li><a href="store.html">Hoodies & Sweatshirts</a></li>
								<li><a href="store.html">Jackets & Coats</a></li>
								<li><a href="store.html">Jeans</a></li>
								<li><a href="store.html">Jewellery</a></li>
								<li><a href="store.html">Jumpers & Cardigans</a></li>
								<li><a href="store.html">Leather Jackets</a></li>
								<li><a href="store.html">Long Sleeve T-Shirts</a></li>
								<li><a href="store.html">Loungewear</a></li>
							</ul>	
						</div>							
					</div>
					<div class="col1">
						<div class="h_nav">
							<ul>
								<li><a href="store.html">Shirts</a></li>
								<li><a href="store.html">Shoes, Boots & Trainers</a></li>
								<li><a href="store.html">Shorts</a></li>
								<li><a href="store.html">Suits & Blazers</a></li>
								<li><a href="store.html">Sunglasses</a></li>
								<li><a href="store.html">Sweatpants</a></li>
								<li><a href="store.html">Swimwear</a></li>
								<li><a href="store.html">Trousers & Chinos</a></li>
								<li><a href="store.html">T-Shirts</a></li>
								<li><a href="store.html">Underwear & Socks</a></li>
								<li><a href="store.html">Vests</a></li>
							</ul>	
						</div>							
					</div>
					<div class="col1">
						<div class="h_nav">
							<h4>Popular Brands</h4>
							<ul>
								<li><a href="store.html">Levis</a></li>
								<li><a href="store.html">Persol</a></li>
								<li><a href="store.html">Nike</a></li>
								<li><a href="store.html">Edwin</a></li>
								<li><a href="store.html">New Balance</a></li>
								<li><a href="store.html">Jack & Jones</a></li>
								<li><a href="store.html">Paul Smith</a></li>
								<li><a href="store.html">Ray-Ban</a></li>
								<li><a href="store.html">Wood Wood</a></li>
							</ul>	
						</div>												
					</div>
				  </div>
				</div>
		    </li>						
	        <li><a class="pink" href="#">Mobile</a></li>
			<li class="grid"><a  href="#">Laptop</a>
				   <div class="megapanel">
					<div class="row">
						<div class="col1">
							<div class="h_nav">
								<ul>
									<li><a href="store.html">Accessories</a></li>
									<li><a href="store.html">Bags</a></li>
									<li><a href="store.html">Caps & Hats</a></li>
									<li><a href="store.html">Hoodies & Sweatshirts</a></li>
									<li><a href="store.html">Jackets & Coats</a></li>
									<li><a href="store.html">Jeans</a></li>
									<li><a href="store.html">Jewellery</a></li>
									<li><a href="store.html">Jumpers & Cardigans</a></li>
									<li><a href="store.html">Leather Jackets</a></li>
									<li><a href="store.html">Long Sleeve T-Shirts</a></li>
									<li><a href="store.html">Loungewear</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<ul>
									<li><a href="store.html">Shirts</a></li>
									<li><a href="store.html">Shoes, Boots & Trainers</a></li>
									<li><a href="store.html">Shorts</a></li>
									<li><a href="store.html">Suits & Blazers</a></li>
									<li><a href="store.html">Sunglasses</a></li>
									<li><a href="store.html">Sweatpants</a></li>
									<li><a href="store.html">Swimwear</a></li>
									<li><a href="store.html">Trousers & Chinos</a></li>
									<li><a href="store.html">T-Shirts</a></li>
									<li><a href="store.html">Underwear & Socks</a></li>
									<li><a href="store.html">Vests</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Popular Brands</h4>
								<ul>
									<li><a href="store.html">Levis</a></li>
									<li><a href="store.html">Persol</a></li>
									<li><a href="store.html">Nike</a></li>
									<li><a href="store.html">Edwin</a></li>
									<li><a href="store.html">New Balance</a></li>
									<li><a href="store.html">Jack & Jones</a></li>
									<li><a href="store.html">Paul Smith</a></li>
									<li><a href="store.html">Ray-Ban</a></li>
									<li><a href="store.html">Wood Wood</a></li>
								</ul>	
							</div>												
						</div>
					  </div>
					</div>
		    </li> --}}
	  </ul> 
	</div>
	
	<div class="clearfix"> </div>
</div>
<!-- <div class="page">
	<h6><a href="#">Page Title</a><b>|</b><span class="for">Page description </span></h6>
</div> -->