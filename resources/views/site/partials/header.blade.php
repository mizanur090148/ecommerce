<!--header-->
<div class="container">
	<div class="header" id="home">	
		<div class="header-para">
			<p>Welcome to our site  <span></span></p>	
		</div>	
		<ul class="header-in">
			@if (Auth::check()) 
			   <li><a href="{{url('/home')}}">DASHBOARD</a> </li>
			    <li><a href="{{url('/favourite-product-list')}}">FAVOURITE PRODUCT</a> </li>
			   <li><a href="{{url('/logout')}}">LOGOUT</a> </li>
			@else
			   <li><a href="{{url('/user/login')}}">LOGIN</a> </li>
			   <li><a href="{{url('/register')}}">REGISTER</a></li>
			   <li><a href="{{ url('/redirect') }}"><img src="{{asset('/site-assets/images/facebok.png')}}" height="35px" width="36px"></a></li>
			   <li><a href="{{ url('/google') }}"><img src="{{asset('/site-assets/images/google.jpg')}}" height="30px" width="30px"></a></li>
				<li><a href="{{ url('/twitter') }}"><img src="{{asset('/site-assets/images/twitter.jpg')}}" height="30px" width="30px"></a></li>
			@endif			
			<li ><a href="#" > CONTACT US</a></li>
		</ul>
		<div class="clearfix"> </div>
	</div>
	<!---->
	<span class="fa-stack fa-lg">
  <i class="fa fa-square-o fa-stack-2x"></i>
  <i class="fa fa-twitter fa-stack-1x"></i>
</span>
	<div class="header-top">
		<div class="logo">
		    <form method="get" action="{{url('/search-product')}}">
			    <input type="text" class="form-control" name="search_key" placeholder="Search" style="height: 36px" value="{{$search_key or ''}}">
			</form>
		</div>
		<div class="header-top-on">
		    {{-- <div class="logo">
				<a href="{{url('/')}}"><img src="{{asset('/site-assets/images/logo.png')}}" alt="" ></a>
			</div>
 --}}
			<ul class="social-in">
				{{-- <li><a href="#"><i> </i></a></li> --}}	
				{{-- <li><a href="#"><img src="{{asset('/site-assets/images/google.jpg')}}" height="35px" width="35px"></a></li>
				<li><a href="#"><img src="{{asset('/site-assets/images/twitter.jpg')}}" height="35px" width="35px"></a></li>
				<li><a href="{{ url('/redirect') }}"><img src="{{asset('/site-assets/images/facebok.png')}}" height="35px" width="35px"></a></li> --}}
				{{-- <li><a href="#"><i class="ic2"> </i></a></li>
				<li><a href="#"><i class="ic3"> </i></a></li> --}}
			</ul>
		</div>
			<div class="clearfix"> </div>
	</div>