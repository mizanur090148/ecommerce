<!DOCTYPE html>
<html>
<head>
<title>{!! $pageTitle or "Site" !!}</title>
<link href="{{asset('/site-assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<!--theme-style-->
<link href="{{asset('/site-assets/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mihstore Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />

<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Cabin:400,500,600,700' rel='stylesheet' type='text/css'>
<!--//fonts-->
</head>
<body> 

        @include('site.partials.header')
		
        @include('site.partials.top-menu')
        
        @yield('content');

        @include('site.partials.right-sidebar')

        @include('site.partials.footer')
		
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script  src="{{asset('/site-assets/js/jquery.min.js')}}"></script>
        <script type="application/x-javascript"> 
		    addEventListener("load", function() {
		        setTimeout(hideURLbar, 0); }, false); 
		        function hideURLbar(){ 
		      	    window.scrollTo(0,1);
		        } 
		</script>
        <!--//slider-script-->
		<script>$(document).ready(function(c) {
			$('.alert-close').on('click', function(c){
				$('.message').fadeOut('slow', function(c){
			  		$('.message').remove();
				});
			});	  
		});
		</script>
		<script>
		$(document).ready(function(c) {
			$('.alert-close1').on('click', function(c){
				$('.message1').fadeOut('slow', function(c){
			  		$('.message1').remove();
				});
			});	  
		});
		</script>
		<script>$(document).ready(function(c) {
			$('.alert-close2').on('click', function(c){
				$('.message2').fadeOut('slow', function(c){
			  		$('.message2').remove();
				});
			});	  
		});
		</script>
		<script type="text/javascript" src="{{asset('/site-assets/js/move-top.js')}}"></script>
		<script type="text/javascript" src="{{asset('/site-assets/js/easing.js')}}"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>	
		<!-- start menu -->
		<link href="{{asset('/site-assets/css/megamenu.css')}}" rel="stylesheet" type="text/css" media="all" />
		<script type="text/javascript" src="{{asset('/site-assets/js/megamenu.js')}}"></script>
		<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>	
       <!--script-->
		<script>
			$(document).ready(function(){
				$(".tab1 .single-bottom").hide();
				$(".tab2 .single-bottom").hide();
				$(".tab3 .w_nav2").hide();
				$(".tab4 .single-bottom").hide();
				$(".tab5 .star-at").hide();
				$(".tab1 ul").click(function(){
					$(".tab1 .single-bottom").slideToggle(300);
					$(".tab2 .single-bottom").hide();
					$(".tab3 .w_nav2").hide();
					$(".tab4 .single-bottom").hide();
					$(".tab5 .star-at").hide();
				})
				$(".tab2 ul").click(function(){
					$(".tab2 .single-bottom").slideToggle(300);
					$(".tab1 .single-bottom").hide();
					$(".tab3 .w_nav2").hide();
					$(".tab4 .single-bottom").hide();
					$(".tab5 .star-at").hide();
				})
				$(".tab3 ul").click(function(){
					$(".tab3 .w_nav2").slideToggle(300);
					$(".tab4 .single-bottom").hide();
					$(".tab5 .star-at").hide();
					$(".tab2 .single-bottom").hide();
					$(".tab1 .single-bottom").hide();
				})
				$(".tab4 ul").click(function(){
					$(".tab4 .single-bottom").slideToggle(300);
					$(".tab5 .star-at").hide();
					$(".tab3 .w_nav2").hide();
					$(".tab2 .single-bottom").hide();
					$(".tab1 .single-bottom").hide();
				})	
				$(".tab5 ul").click(function(){
					$(".tab5 .star-at").slideToggle(300);
					$(".tab4 .single-bottom").hide();
					$(".tab3 .w_nav2").hide();
					$(".tab2 .single-bottom").hide();
					$(".tab1 .single-bottom").hide();
				})	
			});
		</script>
		<!-- script -->


		@yield('footer_script');

</body>
</html>


