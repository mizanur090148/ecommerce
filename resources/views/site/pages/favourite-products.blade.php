@extends('site.layouts.index')
@section('content')

    <div class="content">
		<div class="col-md-9">		
			{{-- <div class="shoe">
				@if(isset($category))
					<img  src="{{asset($category->image)}}" height="110px" width="790px" alt="" >
				@endif	
			</div> --}}
			<div class="content-bottom">
		   {{--  @if(is_numeric(Request::segment(2)))
			    <div class="bottom-grid">
	                <div class="row col-md-9 shoe">
		            	<form class="form-inline" method="post" action="{{url('/view-products/'.Request::segment(2))}}">
		            	    {!! csrf_field() !!}
		            	    <input type="hidden" name="category_id" value="{{Request::segment(2)}}">	
						    <div class="form-group">					   
						        <select class="form-control" name="type" id="search-by-type">
						      	    <option value="">Plz Select One</option>
						      	    <option @if(isset($type)) @if($type=='mostexpensive') selected="selected" @endif @endif value="mostexpensive">Most Expensive</option>
						      	    <option @if(isset($type)) @if($type=='cheapest') selected="selected" @endif @endif value="cheapest">Cheapest</option>						      	    
						        </select>
						    </div>					  
						</form>
		            </div> 			       
	            </div>  
	        @endif --}}
				<div class="bottom-grid">
				@forelse($favorite_products as $prdct)
					<div class="col-md-4 shirt" style="margin-bottom: 15px;margin-top: 15px">
						<div class="bottom-grid-top">
							<a href="{{url('/view-details/'.$prdct->listedAdd->id)}}"><img class="img-responsive" src="{{asset($prdct->listedAdd->feature_image)}}" style="width: 242px;height: 181px" alt="{{$prdct->title}}" >
							<div class="five">
								<!-- <h6 >-50%</h6> -->
							</div>
							<div class="pre">
								<p>{{$prdct->listedAdd->title}}</p>
								<span>${{$prdct->listedAdd->price}}</span>
								<div class="clearfix"> </div>
							</div>
							</a>							
						</div>
						<div class="clearfix"> </div>
					</div>
				@empty
			    	<div class="bottom-grid-top">
						<a><img class="img-responsive" src="{{asset('/site-assets/images/not-found.jpeg')}}" alt="" height="200px" width="200" >
						<div class="five">
						</div>
						<div class="pre">
							<p>Product Not Found</p>
							<div class="clearfix"> </div>
						</div></a>
					</div>
				@endforelse	
			</div>
	    <div class="col-md-12" align="center">
	    @if(isset($products))
	        {!! $products->links() !!}	
	    @endif    		
	    </div>				
		</div>
	</div>	
@endsection

@section('footer_script')
    <script>
		$(document).ready(function(){
			$('#search-by-type').change(function() {
                var type = $('#search-by-type').val();             
                if(type!='') { 
			        $("form").submit(); 
			    }  
		    })	   
		});
    </script>
    <script src="{{asset('slider/js/jssor.slider-22.2.8.min.js')}}" type="text/javascript"></script>    
@endsection