@extends('site.layouts.index')
@section('content')

    <div class="content">
		<div class="col-md-9">		
			<div class="shoe">
				{{-- <img  src="{{asset($category->image)}}" height="110px" width="790px" alt="" > --}}
			</div>
			<div class="content-bottom">
		   {{--  @if(!is_numeric(Request::segment(2))) --}}
			    <div class="bottom-grid">
	                <div class="row col-md-9 shoe">
		            	<form class="form-inline" method="post" action="{{url('/orderby-products/')}}">
		            	    {!! csrf_field() !!}
		            	    <input type="hidden" name="search_key" value="@if(isset($search_key)){{$search_key}}@endif">	
						    <div class="form-group">					   
						        <select class="form-control" name="type" id="search-by-type">
						      	    <option value="">Plz Select One</option>
						      	    <option @if(isset($type)) @if($type=='mostexpensive') selected="selected" @endif @endif value="mostexpensive">Most Expensive</option>
						      	    <option @if(isset($type)) @if($type=='cheapest') selected="selected" @endif @endif value="cheapest">Cheapest</option>	
						        </select>
						    </div>					  
						</form>
		            </div> 			       
	            </div>  
	       {{--  @endif --}}
				<div class="bottom-grid">
				@forelse($products as $prdct)
					<div class="col-md-12 shirt" style="margin-bottom: 5px;margin-top: 5px">
						<div class="bottom-grid-top">
						    <div class="col-md-6 align-left text-right">
								<br/><br/><p><b>{{$prdct->title}}</b></p>
								<span><b>${{$prdct->price}}</b></span>
								<p>{{$prdct->short_description}}</p>
								<div class="clearfix"> </div>
							</div>
                            <div class="col-md-6 align-right"> 
							    <a href="{{url('/view-details/'.$prdct->id)}}"><img class="img-responsive" src="{{asset($prdct->feature_image)}}" style="width: 242px;height: 181px" alt="{{$prdct->title}}" >
							    </a>
							</div>		
						</div>
						<div class="clearfix"> </div>
					</div>
				@empty
			    	<div class="bottom-grid-top">
						<a><img class="img-responsive" src="{{asset('/site-assets/images/not-found.jpeg')}}" alt="" height="200px" width="200" >
						<div class="five">
						</div>
						<div class="pre">
							<p>Product Not Found</p>
							<div class="clearfix"> </div>
						</div></a>
					</div>
				@endforelse	
			</div>
	    <div class="col-md-12" align="center">
	    @if(isset($products))
	        {!! $products->links() !!}	
	    @endif    		
	    </div>				
		</div>
	</div>	
@endsection

@section('footer_script')
    <script>
		$(document).ready(function(){
			$('#search-by-type').change(function() {
                var type = $('#search-by-type').val();             
                if(type!='') { 
			        $("form").submit(); 
			    }  
		    })	   
		});
    </script>
    <script src="{{asset('slider/js/jssor.slider-22.2.8.min.js')}}" type="text/javascript"></script>    
@endsection