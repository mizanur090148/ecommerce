@extends('site.layouts.index')
@section('content')
   <div class="content">
		<div class="col-md-9">
		    <div class="col-md-3 single-top">
		    	<address>
		    		Name: {{$product->user->first_name}} <br/>
		    		Phone: {{$product->contactMobile['phone_number']}} <br/>
		    		City: {{$product->user->city}}
		    	</address>
		    </div>
			<div class="col-md-5 single-top">	
				<ul id="etalage">						
					@forelse($product->images as $pimage)					     
					    <li>
							<img class="etalage_thumb_image img-responsive" src="{{asset($pimage->file_path)}}" alt="" >
							<img class="etalage_source_image img-responsive" src="{{asset($pimage->file_path)}}" alt="" >
						</li>		
					@empty
					    <li>Image Not Found</li>
					@endforelse											
				</ul>
			</div>	
			<div class="col-md-4 single-top-in"> 
				<div class="single-para"> 

				    <div id="loadingDiv">
				        <img id="loading-image" src="{{asset('site-assets/images/loading-img.png')}}" style="display:none;"/>
				    </div>
				    <div id="message"></div>

				    <b>Category:</b> {{$product->category['name']}} -> {{$product->brand['name']}}
				    <br/></br/>
				    <h4>{{$product->title or ''}}</h4>
					<p>{{$product->short_description or ''}}</p>
					
					<div class="para-grid">
						<span  class="add-to">${{$product->price or ''}}</span>
						@if(Auth::check())
						    <a href="#" class=" cart-to add-fevorite-btn" product-id="{{$product->id}}"> Add favourite</a>
						@endif					
						<div class="clearfix"></div>
					 </div>
					<b>Brand:</b> {{$product->brand['name']}}

					<p><b>Features: </b>
					   @forelse($features as $fetur)
                           {{$fetur->name}}, 
                       @empty
                       @endforelse 
					</p>

				{{--	
					<div class="available">
						<h6>Available Options :</h6>						
				    </div>					
						<a href="#" class="cart-an ">More details</a>
					<div class="share">
					<h4>Share Product :</h4>
					<ul class="share_nav">
						<li><a href="#"><img src="{{asset('/site-assets/images/facebook.png')}}" title="facebook"></a></li>
						<li><a href="#"><img src="{{asset('/site-assets/images/twitter.png')}}" title="Twiiter"></a></li>
						<li><a href="#"><img src="{{asset('/site-assets/images/rss.png')}}" title="Rss"></a></li>
						<li><a href="#"><img src="{{asset('/site-assets/images/gpluse.png')}}" title="Google+"></a></li>
		    		</ul> 
				</div> --}}
				</div>
			</div>
		<div class="clearfix"> </div>
        <ul id="flexiselDemo1">
			@forelse($all_products as $prdct)
			<li style="margin: 50px"><a href="{{url('/view-details/'.$prdct->id)}}"><img src="{{asset($prdct->feature_image)}}" style="width: 242px;height: 181px; margin: 10px" /><div class="grid-flex"><a href="#">{{ $prdct->title}}</a><p>{{ $prdct->price}}</p></div></a></li>
			@empty
			   Not Found
			@endforelse   
		</ul>
	</div>	
   
@endsection
@section('footer_script')   
        <!--add favourite product-->		 
    <script type="text/javascript">
	   $(window).load(function() {
		    $("#flexiselDemo1").flexisel({
				visibleItems: 5,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
		    	responsiveBreakpoints: { 
		    		portrait: { 
		    			changePoint:480,
		    			visibleItems: 1
		    		}, 
		    		landscape: { 
		    			changePoint:640,
		    			visibleItems: 2
		    		},
		    		tablet: { 
		    			changePoint:768,
		    			visibleItems: 3
		    		}
		    	}
		    });		    
		});
	</script>
    <script type="text/javascript" src="{{asset('/site-assets/js/jquery.flexisel.js')}}"></script>
	<link rel="stylesheet" href="{{asset('/site-assets/css/etalage.css')}}">
	<script src="{{asset('/site-assets/js/jquery.etalage.min.js')}}"></script>
	<script>
		jQuery(document).ready(function($){
			$('#etalage').etalage({
				thumb_image_width: 300,
				thumb_image_height: 400,
				source_image_width: 900,
				source_image_height: 1200,
				show_hint: true,
				click_callback: function(image_anchor, instance_id){
					alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
				}
			});
		});
	</script>
	<script type="text/javascript" src="{{asset('/site-assets/js/move-top.js')}}"></script>
	<script type="text/javascript" src="{{asset('/site-assets/js/easing.js')}}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<script type="text/javascript"> 
		$(document).ready(function(){ 
            $(".add-fevorite-btn").click(function() {
            	$("#loading-image").show();
            	var product_id =  $(this).attr("product-id");
            	if(product_id!=null) {
            	    var token = "{{ csrf_token() }}";
	            	$.ajax({
	            		url: "{{url('/add-to-fevorite')}}",
	            		type: "post",
	            		data: {product_id:product_id,_token:token},
	            		success: function(returnMsg) {
	            			$("#loading-image").hide();
                            if (returnMsg==0) {                         
	                            $('#message')
			                        .addClass('alert alert-success text-center')
			                        .text('Sccessfully Added to your favourite list')
			                        .fadeIn().delay(2000).fadeOut(2000);  
			                }else if (returnMsg==1) {
			                        $('#message')
			                        .addClass('alert alert-danger text-center')
			                        .text('Not successfully Added')
			                        .fadeIn().delay(2000).fadeOut(2000);  
			                }else {
                                    $('#message')
			                        .addClass('alert alert-danger text-center')
			                        .text('Already exist this product')
			                        .fadeIn().delay(2000).fadeOut(2000);  
			                } 
	            		}
	            	});
            	}
            })
		});
	</script>	

@endsection