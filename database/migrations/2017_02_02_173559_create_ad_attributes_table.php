<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_attributes', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('name')->unique();
            $table->integer('category_id')->unsigned();
            $table->tinyInteger('is_deleted');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('ad_categories')->onDelete('cascade');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ad_attributes');
    }
}
