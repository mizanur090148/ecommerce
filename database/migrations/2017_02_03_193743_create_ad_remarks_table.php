<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdRemarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_remarks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('listed_ad_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('comment');
            $table->tinyInteger('is_deleted');  
            $table->timestamps();

            $table->foreign('listed_ad_id')->references('id')->on('listed_ads')->onDelete('cascade'); 
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ad_remarks');
    }
}
