<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListedAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listed_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('ad_category_id')->unsigned();
            $table->integer('brand_id')->unsigned()->nullable();
            $table->string('other')->nullable();
            $table->integer('ad_type')->nullable();
            $table->integer('created_by')->unsigned();
            $table->string('short_description')->nullable();
            $table->mediumText('long_description')->nullable();
            $table->enum('is_sold',['yes','no'])->default('no');
            $table->string('condition')->nullable();
            $table->decimal('price', 10, 0);
            $table->enum('negotiable',['yes','no'])->default('no');
            $table->string('location');
            $table->enum('number_status',['yes','no'])->default('yes');
            $table->string('feature_image')->nullable();
            $table->string('email');  
            $table->string('company_name')->nullable();
            $table->tinyInteger('is_deleted');       
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade'); 
            $table->foreign('ad_category_id')->references('id')->on('ad_categories')->onDelete('cascade');
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listed_ads');
    }
}
