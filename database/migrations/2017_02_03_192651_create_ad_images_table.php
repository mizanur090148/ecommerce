<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('listed_ad_id')->unsigned();
            $table->string('file_path');
            $table->tinyInteger('is_deleted');
            $table->timestamps();

            $table->foreign('listed_ad_id')->references('id')->on('listed_ads')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ad_images');
    }
}
