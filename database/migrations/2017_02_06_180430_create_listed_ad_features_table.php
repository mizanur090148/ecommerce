<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListedAdFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listed_ad_features', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('listed_ad_id')->unsigned();
            $table->integer('category_feature_id')->unsigned();
            $table->timestamps();

            $table->foreign('listed_ad_id')->references('id')->on('listed_ads')->onDelete('cascade'); 
            $table->foreign('category_feature_id')->references('id')->on('category_features')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listed_ad_features');
    }
}
