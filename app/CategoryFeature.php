<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryFeature extends Model
{
    protected $fillable = ['ad_category_id','name'];
}
