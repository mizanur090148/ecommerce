<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactMobile extends Model
{
    protected $fillable = ['listed_ad_id','phone_number'];
}
