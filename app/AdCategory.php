<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//use App\Brand;

class AdCategory extends Model
{    
    protected $fillable = ['name','parent','sort','is_deleted'];

    
    /*
    *one category to many brands 
    */
    public function brands()
    {
    	return $this->hasMany('App\Brand');
    }

    /*
    *one category to many features 
    */
    public function categoryFeature()
    {
    	return $this->hasMany('App\CategoryFeature');
    }
}
