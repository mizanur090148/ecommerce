<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdAttribute extends Model
{    
    protected $fillable = ['name','category_id','is_deleted'];
}
