<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitedByUser extends Model
{
    protected $fillable = ['listed_ad_id','user_id','count'];
}
