<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\ListedAd;
use App\AdCategory;
use App\Brand;
use App\Feature;
use App\CategoryFeature;
use App\ListedAdFeature;
use App\Condition;
use App\ContactMobile;
use App\AdImages;
use App\FavoriteAd;
use Auth,DB,Session,Validator;

class ListedAddController extends Controller
{
    
    protected $file_path;
    protected $file_store_path;   

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function __construct()
    {
        $this->middleware('auth');

        $this->file_path = public_path('uploads/product_images');       
        $this->file_store_path = '/uploads/product_images/'; 
    }

    public function addListedAdd()
    {
        $pageTitle = 'Listed add';
        $categories = AdCategory::select('id','name')->get(); 
        $brands = Brand::select('id','name')->get();
        $conditions = Condition::all();

    	return view('listed_add', [
            'categories' => $categories,
            'brands' => $brands,
            'conditions' => $conditions,
            'pageTitle' => $pageTitle           
    	]);    		                    
    }

    public function getBrandAndFeature(Request $request)
    {    	
    	$brands = Brand::where('ad_category_id',$request->ad_category_id)->get();
    	$category_features = CategoryFeature::where('ad_category_id',$request->ad_category_id)->get();

    	return response()->json([
		    'brands' => $brands,
		    'category_features' => $category_features
		]);
    	
    }

    public function addListedAddPost(Request $request)
    {       
        //dd($request->file('images'));
        $rules = [
            'title' => 'required|min:2',
            'ad_category_id' => 'required',
            'price' => 'required',
            'condition'=> 'required',
            'email' => 'required|email'
        ];

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){ //dd($validator);

            return redirect()->back()->withErrors($validator)->withInput();
        }  

        $store_feature_image = null;
        $feature_image = $request->file('feature_image');
        if ($feature_image != null) {
            
                $store_feature = preg_replace("/[^a-zA-Z]+/", "", strtolower($request->title)).'-'.time().'.' . $feature_image->getClientOriginalExtension(); 
                $feature_image->move($this->file_path, $store_feature);
                $store_feature_image = $this->file_store_path.$store_feature;
        }
        
        /*$product_images = $request->file('images');
        if ($product_images != null) {

            $images_name = [];
            foreach ($product_images as $image) {
                $rules = array('images' => 'required|mimes:png,jpeg|max:20');
                $validator = Validator::make(array('images' => $image), $rules);

                if ($validator->passes()) {
                   
                    $imagename = $title.'-'.time().'.' . $image->getClientOriginalExtension(); 
                    $file->move($this->file_path, $imagename); 
                    $images_name[] .= $imagename;
                }
            }
        } */

        $product_images = $request->file('images');
        if ($product_images != null) {
            $i = 0;
            $images_name = [];
            foreach ($product_images as $image) {
                if ($image != null) {
                    $i++;
                    $imagename = preg_replace("/[^a-zA-Z]+/", "", strtolower($request->title)).$i.'-'.time().'.' . $image->getClientOriginalExtension(); 
                    $image->move($this->file_path, $imagename); 
                    $images_name[] .= $imagename;
                }
            }
        } 
        
        DB::beginTransaction();
        try {    
		        
		        $listed_add = new ListedAd();
                $listed_add->title = $request->title;
		        $listed_add->ad_category_id = $request->ad_category_id;
		        $listed_add->created_by = Auth::user()->id;
		        $listed_add->location = $request->location;
		        $listed_add->condition = $request->condition;
		        $listed_add->brand_id = $request->brand_id;
		        $listed_add->other = $request->other;
		        $listed_add->short_description = $request->short_description;
		        $listed_add->price = $request->price;
                $listed_add->feature_image = $store_feature_image;
		        $listed_add->negotiable = $request->negotiable;		        
		        $listed_add->email = $request->email; 
		        if($listed_add->save()) {					

					if($request->feature_id != null) {
						foreach ($request->feature_id as $feture_id) {
							$feature = new ListedAdFeature();
							$feature->listed_ad_id = $listed_add->id;
							$feature->category_feature_id = $feture_id;
							$feature->save();   
						}
				    } 	

				    if($request->phone_number != null) {
						foreach ($request->phone_number as $number) {
							$mobile = new ContactMobile();
							$mobile->listed_ad_id = $listed_add->id;
							$mobile->phone_number = $number;
							//$mobile->status = $status;
							$mobile->save();   
						}
				    } 	

                    if(count($images_name>0)) {
                        foreach ($images_name as $img) {
                            $add_image = new AdImages();
                            $add_image->listed_ad_id = $listed_add->id;
                            $add_image->file_path = $this->file_store_path.$img;                           
                            $add_image->save();   
                        }
                    }   

		        	Session::flash('success','Successfully Added');            
		        } else {
		            Session::flash('error','Not Successfully Added');
		        } 

		        DB::commit();

		    } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
	            DB::rollback();	            
	            Session::flash('error', $e->getMessage());
            }    

        return Redirect()->back();       
    }

    public function favouriteProductList()
    {
        $pageTitle = 'Favourite Product List';
         $favorite_products = FavoriteAd::with('listedAdd')->where('user_id',Auth::user()->id)->get();      
        return view('site.pages.favourite-products')->with(['favorite_products'=>$favorite_products,'pageTitle'=>$pageTitle]);
    }
}
