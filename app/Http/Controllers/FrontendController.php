<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\AdCategory;
use App\ListedAd;
use App\FavoriteAd;
use App\ProductViewCount;
use App\User;
use App\VisitedByUser;
use App\Slider;
use Auth,View;

class FrontendController extends Controller
{
    
   /* public function index()
        {
        	$categories = AdCategory::all();    	
        	return view('frontend.pages.home')->with('categories',$categories);
        }
    */
    public function home()
    {
    	$pageTitle = 'Home';
        $products = ListedAd::with('images','contactMobile')->orderBy('id','desc')->get();
        $most_view_products = ListedAd::join('product_view_counts','listed_ads.id','=','product_view_counts.listed_ad_id')
            ->groupBy('product_view_counts.listed_ad_id')
            ->orderBy('product_view_counts.count','desc')
            ->get(['listed_ads.*']); 

        $own_views_product = ListedAd::join('visited_by_users','listed_ads.id','=','visited_by_users.listed_ad_id')
            ->groupBy('visited_by_users.listed_ad_id')
            ->orderBy('visited_by_users.id','desc')
            ->get(['listed_ads.*']);    

        $sliders = Slider::orderBy('position','asc')->get();

        return view('site.pages.home',[    		 
    		    'products' => $products,
                'pageTitle' => $pageTitle,
                'most_view_products' => $most_view_products, 
                'own_views_product' => $own_views_product,
                'sliders' => $sliders
    		]);

    }

    public function searchByProductType(Request $request)
    { 
        if($request->type=='mostexpensive') {
            $products = ListedAd::with('images','contactMobile')->where('ad_category_id',$request->category_id)->orderBy('price','desc')->paginate(9);        
        }elseif($request->type=='cheapest') {
            $products = ListedAd::where('ad_category_id',$request->category_id)->orderBy('price','asc')->paginate(9);
        }

        return view('site.pages.category-wise-products',[          
                'products' => $products,
                'type' => $request->type
            ]);
        
    }
    public function viewDetails($id)
    {  
    	$pageTitle = 'View details';
        $product = ListedAd::where('id',$id)->with('images','contactMobile','user','category')->first();

        $features = ListedAd::join('category_features','category_features.ad_category_id','=','listed_ads.ad_category_id')                    
                    ->where('listed_ads.id',$id)
                    ->get(['category_features.name']);                   
              
        $all_products = ListedAd::where('ad_category_id',$product->ad_category_id)->get(); 
        
        $views_count = ProductViewCount::where('listed_ad_id',$id)->count();
        if($views_count>0) {
            ProductViewCount::where('listed_ad_id',$id)->increment('count');
        }else{
            ProductViewCount::create(['listed_ad_id'=>$id,'count'=>1]); 
        }

        if(Auth::check()) {
            VisitedByUser::create(['listed_ad_id'=>$id,'user_id'=>Auth::user()->id,'count'=>1]);
        }  
        
        return view('site.pages.view-product-details',[          
                'product' => $product,
                'all_products' => $all_products,
                'features' => $features,
                'pageTitle' => $pageTitle
            ]);
    }

    public function addToFavoriteProduct(Request $request)
    {  
        $find_favorite = FavoriteAd::where(['listed_ad_id'=>$request->product_id,'user_id'=>Auth::user()->id])->count();
      
        if($find_favorite<1) {        
            $favorite = new FavoriteAd();
            $favorite->listed_ad_id = $request->product_id;
            $favorite->user_id = Auth::user()->id;
            if($favorite->save()) {
                $message = 0;
            } else {
                $message = 1;
            }            
        }else {
            $message = 2;
        } 
        return $message;    
    }

    public function viewCategoryWiseProduct($ad_category_id)
    {       
        $pageTitle = 'Category wise products';
        $category = AdCategory::where('id',$ad_category_id)->first();
        $products = ListedAd::with('images','contactMobile')->where('ad_category_id',$ad_category_id)->paginate(9);
    	return view('site.pages.category-wise-products',[    		 
    		    'products' => $products,
                'pageTitle' => $pageTitle,
                'category' => $category
    		]);     
    }

    public function searchProduct(Request $request)
    { 
        
        $products = ListedAd::where('title','LIKE','%'.$request->search_key.'%')->paginate(9);       
        
        return view('site.pages.search-result-view',[          
                'products' => $products,
                'search_key' => $request->search_key               
            ]);
    }

    public function orderByProduct(Request $request)
    {                

        if($request->type=='mostexpensive') {
            $products = ListedAd::where('title','LIKE','%'.$request->search_key.'%')->orderBy('price','desc')->paginate(9);            
        }elseif($request->type=='cheapest') {
            $products = ListedAd::where('title','LIKE','%'.$request->search_key.'%')->orderBy('price','asc')->paginate(9); 
        }

        return view('site.pages.search-result-view',[          
                'products' => $products,
                'search_key' => $request->search_key,
                'type' => $request->type               
            ]);        
    }

}
