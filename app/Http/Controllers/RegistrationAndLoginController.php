<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth,Validator,Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\User;

class RegistrationAndLoginController extends Controller
{
    
    public function register()
    {
       $pageTitle = 'Register';   
       return view('auth.register')->with('pageTitle',$pageTitle);
    }
    
    public function registerPost(Request $request)
    {   

        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'g-recaptcha-response' => 'required|recaptcha',
        ];

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){ //dd($validator);

            return redirect()->back()->withErrors($validator)->withInput();
        }  
       
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        /*Mail::send('site.registration.pages.user-signup-email', $data, function($message) use ($data) {
            $message->to($data['email'], $data['name'])
                ->subject($data['subject']);
        });*/

         Session::flash('message','Successfully created');
        return view('auth.login');

    }


    public function login()
    {       
        return view('auth.login')->with('pageTitle','Login');
    }

    public function postLogin(Request $request)
    {
        
       /*  if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            return redirect()->intended('home');
        }*/

        $userLogin = [
            'email' => $request->email,
            'password' => $request->password
        ]; 
        if (Auth::attempt($userLogin)) {              
            return redirect()->intended('home');
        } else {
            return redirect()->back();
        }
    }

    public function logout()
    {
        Auth::logout();        
        return view('auth.login')->with('pageTitle','Login');
    }
}
