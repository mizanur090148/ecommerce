<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Socialite;
use Auth;
use App\User;

class SocialAuthController extends Controller
{   
    public function redirect() {
        return Socialite::driver('facebook')->redirect();   
    }    

    public function callback() 
    {      
        try {          
                $user = Socialite::driver('facebook')->user(); 
                if($user) { 
                    $existAccount = User::where('email', $user->email)->first();  
                    if($existAccount) { 
                        Auth::loginUsingId($existAccount->id);
                    }else {  
                        $userModel = new User();
                        $userModel->first_name = $user->name;
                        $userModel->email = $user->email;
                        $userModel->password = bcrypt($user->name);
                        $userModel->save();

                        Auth::loginUsingId($userModel->id);
                    }               
                    return redirect('home'); 
                }
            } catch(\Exception $e){
                session()->flash('error', 'These credentials do not match our records.');
               return redirect('user/login');
           } 
    }   

    public function createOrGetUser($fbuser)
    { 
        $existData = [
            'email' => $fbuser->email,
            'password' => $fbuser->name
        ];     

        $account = User::where('email', $fbuser->email)->first();
        if ($account) {
            return $existData;
        } else {
            $input = [
                'first_name' => $fbuser->name,
                'email' => $fbuser->email,
                'password' => $fbuser->name
            ];        
            
           User::create($input);
            return $existData;
        }
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();       
    }

    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
            if($user) {   

                $existAccount = User::where('email', $user->email)->first();  
                if($existAccount) { 
                    Auth::loginUsingId($existAccount->id);
                }else {  
                    $userModel = new User();
                    $userModel->first_name = $user->name;
                    $userModel->email = $user->email;
                    $userModel->password = bcrypt($user->name);
                    $userModel->save();

                    Auth::loginUsingId($userModel->id);
                }               
                return redirect('home');
            }            
        } catch (Exception $e) {
            session()->flash('error', 'These credentials do not match our records.');
            return redirect('user/login');
        }
    }

    public function redirectToTwitter()
    {
        return Socialite::driver('twitter')->redirect();       
    }

    public function handleTwitterCallback()
    {
        try {
            $user = Socialite::driver('twitter')->user();           
            if($user) {   

                $existAccount = User::where('email', $user->email)->first();  
                if($existAccount) { 
                    Auth::loginUsingId($existAccount->id);
                }else {  
                    $userModel = new User();
                    $userModel->first_name = $user->name;
                    $userModel->email = $user->email;
                    $userModel->password = bcrypt($user->name);
                    $userModel->save();

                    Auth::loginUsingId($userModel->id);
                }               
                return redirect('home');
            }            
        } catch (Exception $e) {
            session()->flash('error', 'These credentials do not match our records.');
            return redirect('user/login');
        }
    }

    public function redirectToPinterest()
    {
        return Socialite::with('pinterest')->redirect();       
    }

    public function handlePinterestCallback()
    {
        try {
            $user = Socialite::with('pinterest')->user();  
            dd($user);         
            if($user) {   

                $existAccount = User::where('email', $user->email)->first();  
                if($existAccount) { 
                    Auth::loginUsingId($existAccount->id);
                }else {  
                    $userModel = new User();
                    $userModel->first_name = $user->name;
                    $userModel->email = $user->email;
                    $userModel->password = bcrypt($user->name);
                    $userModel->save();

                    Auth::loginUsingId($userModel->id);
                }               
                return redirect('home');
            }            
        } catch (Exception $e) {
            session()->flash('error', 'These credentials do not match our records.');
            return redirect('user/login');
        }
    }


}
