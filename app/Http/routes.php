<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middlewareGroups' => 'web'], function () {

	/*Route::get('/', function () {
	    return view('welcome');
	});*/

	#Route::auth();

    
	Route::get('user/login','RegistrationAndLoginController@login');
	Route::get('user/register','RegistrationAndLoginController@register');
	Route::post('user/register-post','RegistrationAndLoginController@registerPost');
	Route::post('/login/post','RegistrationAndLoginController@postLogin');
	Route::get('logout','RegistrationAndLoginController@logout');


	Route::get('/home', 'HomeController@index');
	Route::get('/search-product','FrontendController@searchProduct');

	Route::post('/get-brand-feature','ListedAddController@getBrandAndFeature');

	Route::get('/listed-ad','ListedAddController@addListedAdd');
	Route::post('/listed-ad-post','ListedAddController@addListedAddPost');
	Route::get('/favourite-product-list','ListedAddController@favouriteProductList');

	## Social Auth Route
	Route::get('/redirect', 'SocialAuthController@redirect');
	Route::get('/callback', 'SocialAuthController@callback');

	Route::get('/google', 'SocialAuthController@redirectToGoogle');
    Route::get('/callback/google', 'SocialAuthController@handleGoogleCallback');

    Route::get('/twitter', 'SocialAuthController@redirectToTwitter');
    Route::get('/callback/twitter', 'SocialAuthController@handleTwitterCallback');

    Route::get('/pinterest', 'SocialAuthController@redirectToPinterest');
    Route::get('/callback/pinterest', 'SocialAuthController@handlePinterestCallback');
	#

	Route::get('/', 'FrontendController@home'); 
	Route::get('/view-details/{id}','FrontendController@viewDetails');
	Route::get('/index', 'FrontendController@index');
	Route::get('/view-products/{ad_category_id}','FrontendController@viewCategoryWiseProduct');
	Route::post('/add-to-fevorite','FrontendController@addToFavoriteProduct');

	Route::post('/view-products/{ad_category_id}','FrontendController@searchByProductType');
	Route::post('/search-type-post', 'FrontendController@searchByProductType');
	Route::post('/orderby-products', 'FrontendController@orderByProduct');

});