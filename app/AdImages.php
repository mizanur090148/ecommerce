<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdImages extends Model
{
    protected $fillable = ['listed_ad_id','file_path','is_deleted'];
}
