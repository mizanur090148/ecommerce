<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListedAd extends Model
{
    protected $fillable = [
         'title','category_id','brand_id','ad_type','created_by','short_description','long_description','feature_image','is_sold','condition','price','negotiable','location','phone_number','email','is_deleted'
    ];  

    public function images()
    {
    	return $this->hasMany('App\AdImages');
    }  
    public function contactMobile()
    {
    	return $this->hasOne('App\ContactMobile');
    }
    public function user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
    public function category()
    {
        return $this->belongsTo('App\AdCategory','ad_category_id','id');
    }
    public function feature()
    {
        return $this->hasMany('App\Feature','id','ad_category_id');
    }

    public static function test($id) {
      return User::first()->email;
    }
   
}
