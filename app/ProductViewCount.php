<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//use App\Brand;

class ProductViewCount extends Model
{    
    
    protected $table = 'product_view_counts';
    protected $fillable = ['listed_ad_id','count'];
    
}
