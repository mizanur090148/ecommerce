<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{   
    protected $fillable = ['name','ad_category_id','website','address'];
}
