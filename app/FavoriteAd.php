<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteAd extends Model
{
    protected $fillable = ['listed_ad_id'];

    public function listedAdd()
    {
        return $this->belongsTo('App\ListedAd','listed_ad_id','id');
    }
}
