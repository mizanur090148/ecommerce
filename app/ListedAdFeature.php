<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListedAdFeature extends Model
{
    protected $fillable = ['category_feature_id','listed_ad_id'];
}
