<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\AdCategory;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $categories = AdCategory::all(); 
        View::share('categories', $categories);      
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
