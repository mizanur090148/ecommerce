<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdRemark extends Model
{
    protected $fillable = ['listed_ad_id','user_id','comment','is_deleted'];
}
